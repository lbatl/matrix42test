package com.matrix42.matrix42test.model;

import com.matrix42.matrix42test.network.response.profile.ProfileResponse;

import okhttp3.ResponseBody;
import rx.Observable;

/**
 * Created by superuser on 20.10.16.
 */

public interface DataManager {
    Observable<ProfileResponse> getUserProfile();
}
