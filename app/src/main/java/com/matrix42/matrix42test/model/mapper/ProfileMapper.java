package com.matrix42.matrix42test.model.mapper;

import com.matrix42.matrix42test.model.data.ProfileData;
import com.matrix42.matrix42test.network.response.profile.ProfileResponse;

import javax.inject.Inject;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by superuser on 20.10.16.
 */

public class ProfileMapper implements Func1<ProfileResponse, ProfileData> {
    @Inject
    public ProfileMapper() {

    }

    @Override
    public ProfileData call(ProfileResponse profileResponse) {
        if (profileResponse == null) return null;

        return Observable.just(profileResponse)
                .map(response -> {
                    return new ProfileData(response.firstName == null ? "" : response.firstName,
                            response.lastName == null ? "" : response.lastName,
                            response.salutation == null ? "" : response.salutation,
                            response.mailAddress == null ? "" : response.mailAddress,
                            response.avatarSasUrl == null ? "" : response.avatarSasUrl,
                            response.companyName == null ? "" : response.companyName,
                            response.country == null ? "" : response.country,
                            response.phone == null ? "" : response.phone,
                            response.city == null ? "" : response.city,
                            response.zipCode == null ? "" : response.zipCode,
                            response.street == null ? "" : response.street,
                            response.street2 == null ? "" : response.street2,
                            response.cultureString == null ? "" : response.cultureString
                            );
                }).toBlocking().first();
    }
}
