package com.matrix42.matrix42test.model;


import com.matrix42.matrix42test.App;
import com.matrix42.matrix42test.network.ApiInterface;
import com.matrix42.matrix42test.network.response.profile.ProfileResponse;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Provides;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.Scheduler;

/**
 * Created by superuser on 20.10.16.
 */

public class DataManagerImpl implements DataManager {


    @Inject
    ApiInterface apiInterface;

    @Inject
    @Named("UI_THREAD")
    Scheduler uiThread;


    @Inject
    @Named("IO_THREAD")
    Scheduler ioThread;

    public DataManagerImpl() {
        App.appComponent.inject(this);
    }

    <T> Observable.Transformer<T, T> applySchedulers() {
        return observable -> observable.subscribeOn(ioThread)
                .observeOn(uiThread);
    }


    @Override
    public Observable<ProfileResponse> getUserProfile() {
        return apiInterface.getUserProfile().compose(applySchedulers());
    }
}
