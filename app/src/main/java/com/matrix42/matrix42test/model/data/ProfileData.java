package com.matrix42.matrix42test.model.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by superuser on 20.10.16.
 */

public class ProfileData implements Parcelable {


    private String firstName;
    private String lastName;
    private String salutation;
    private String email;
    private String avaUrl;
    private String companyName;
    private String country;
    private String phone;
    private String city;
    private String zipCode;
    private String street;
    private String additionalInfo;
    private String language;

    public ProfileData(String firstName, String lastName, String salutation, String email, String avaUrl, String companyName, String country, String phone, String city, String zipCode, String street, String additionalInfo, String language) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.salutation = salutation;
        this.email = email;
        this.avaUrl = avaUrl;
        this.companyName = companyName;
        this.country = country;
        this.phone = phone;
        this.city = city;
        this.zipCode = zipCode;
        this.street = street;
        this.additionalInfo = additionalInfo;
        this.language = language;
    }

    protected ProfileData(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
        salutation = in.readString();
        email = in.readString();
        avaUrl = in.readString();
        companyName = in.readString();
        country = in.readString();
        phone = in.readString();
        city = in.readString();
        zipCode = in.readString();
        street = in.readString();
        additionalInfo = in.readString();
        language = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(salutation);
        dest.writeString(email);
        dest.writeString(avaUrl);
        dest.writeString(companyName);
        dest.writeString(country);
        dest.writeString(phone);
        dest.writeString(city);
        dest.writeString(zipCode);
        dest.writeString(street);
        dest.writeString(additionalInfo);
        dest.writeString(language);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProfileData> CREATOR = new Creator<ProfileData>() {
        @Override
        public ProfileData createFromParcel(Parcel in) {
            return new ProfileData(in);
        }

        @Override
        public ProfileData[] newArray(int size) {
            return new ProfileData[size];
        }
    };

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSalutation() {
        return salutation;
    }

    public String getEmail() {
        return email;
    }

    public String getAvaUrl() {
        return avaUrl;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getCountry() {
        return country;
    }

    public String getPhone() {
        return phone;
    }

    public String getCity() {
        return city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getStreet() {
        return street;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public String getLanguage() {
        return language;
    }
}
