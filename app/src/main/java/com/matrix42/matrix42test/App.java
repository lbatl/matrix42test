package com.matrix42.matrix42test;

import android.app.Application;

import com.matrix42.matrix42test.di.components.AppComponent;
import com.matrix42.matrix42test.di.components.DaggerAppComponent;

/**
 * Created by superuser on 20.10.16.
 */

public class App extends Application {

    private static App instance;
    public static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        instance=this;
        buildGraphAndInject();
    }

    public static App getInstance()
    {
        return instance;
    }

    private void buildGraphAndInject() {
        appComponent = DaggerAppComponent.builder().build();
    }
}



