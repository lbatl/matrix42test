
package com.matrix42.matrix42test.network.response.profile;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EnterpriseAccount {

    @SerializedName("Id")
    @Expose
    public String id;
    @SerializedName("AccountName")
    @Expose
    public String accountName;
    @SerializedName("CustomerNumber")
    @Expose
    public Object customerNumber;
    @SerializedName("IsValid")
    @Expose
    public boolean isValid;
    @SerializedName("LogoSasUrl")
    @Expose
    public Object logoSasUrl;
    @SerializedName("Enabled")
    @Expose
    public boolean enabled;
    @SerializedName("Created")
    @Expose
    public String created;

}
