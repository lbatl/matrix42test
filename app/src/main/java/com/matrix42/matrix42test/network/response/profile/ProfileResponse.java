
package com.matrix42.matrix42test.network.response.profile;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileResponse {

    @SerializedName("MailAddress")
    @Expose
    public String mailAddress;
    @SerializedName("Salutation")
    @Expose
    public String salutation;
    @SerializedName("FirstName")
    @Expose
    public String firstName;
    @SerializedName("LastName")
    @Expose
    public String lastName;
    @SerializedName("CompanyName")
    @Expose
    public String companyName;
    @SerializedName("Street")
    @Expose
    public String street;
    @SerializedName("Street2")
    @Expose
    public String street2;
    @SerializedName("City")
    @Expose
    public String city;
    @SerializedName("ZipCode")
    @Expose
    public String zipCode;
    @SerializedName("Country")
    @Expose
    public String country;
    @SerializedName("Phone")
    @Expose
    public String phone;
    @SerializedName("Password")
    @Expose
    public Object password;
    @SerializedName("AvatarSasUrl")
    @Expose
    public String avatarSasUrl;
    @SerializedName("CultureString")
    @Expose
    public String cultureString;
    @SerializedName("ActivationHandler")
    @Expose
    public Object activationHandler;
    @SerializedName("IsValid")
    @Expose
    public boolean isValid;
    @SerializedName("IsLocked")
    @Expose
    public boolean isLocked;
    @SerializedName("IsActivated")
    @Expose
    public boolean isActivated;
    @SerializedName("Created")
    @Expose
    public String created;
    @SerializedName("ChangePasswordOnNextLogin")
    @Expose
    public boolean changePasswordOnNextLogin;
    @SerializedName("IsInOrganization")
    @Expose
    public boolean isInOrganization;
    @SerializedName("EnterpriseAccount")
    @Expose
    public EnterpriseAccount enterpriseAccount;

}
