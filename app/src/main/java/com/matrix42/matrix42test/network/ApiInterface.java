package com.matrix42.matrix42test.network;

import com.matrix42.matrix42test.network.response.profile.ProfileResponse;

import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by superuser on 20.10.16.
 */

public interface ApiInterface {
    @GET("/api/session/profile")
    Observable<ProfileResponse> getUserProfile();
}
