package com.matrix42.matrix42test.network;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.matrix42.matrix42test.App;
import com.matrix42.matrix42test.R;
import com.matrix42.matrix42test.utils.SharedPrefsHelper;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by superuser on 20.10.16.
 */

public class ApiModule {

    public static ApiInterface getApiInterface() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(60, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS);
        httpClient.addInterceptor(logging);


        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Request.Builder requestBuilder = chain.request().newBuilder();
                String authToken = SharedPrefsHelper.getUserAuth();
                if (!authToken.equals("")) {
                    requestBuilder.addHeader("Authorization", String.format(App.getInstance().getResources().getString(R.string.auth_header_format), authToken));
                }

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });


        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://accounts.matrix42.com")
                .client(httpClient.build())
                .addConverterFactory(buildGsonConverter())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create());


        return builder.build().create(ApiInterface.class);
    }


    private static GsonConverterFactory buildGsonConverter() {
        GsonBuilder gsonBuilder = new GsonBuilder();

        // Adding custom deserializers
        //gsonBuilder.registerTypeAdapter(SomeClass.class, SomeDeserializer())
        Gson myGson = gsonBuilder.create();

        return GsonConverterFactory.create(myGson);
    }

}
