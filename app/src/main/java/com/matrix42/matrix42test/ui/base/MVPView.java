package com.matrix42.matrix42test.ui.base;


public interface MVPView
{
    void showContent(boolean showContent);
    void showLoading();
    void hideLoading();
    void showError(Throwable throwable);
}
