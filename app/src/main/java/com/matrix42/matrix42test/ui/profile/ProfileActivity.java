package com.matrix42.matrix42test.ui.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.matrix42.matrix42test.R;
import com.matrix42.matrix42test.di.components.DaggerProfileComponent;
import com.matrix42.matrix42test.di.components.ProfileComponent;
import com.matrix42.matrix42test.di.modules.ProfileModule;
import com.matrix42.matrix42test.model.data.ProfileData;
import com.matrix42.matrix42test.ui.base.BaseActivity;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by superuser on 20.10.16.
 */

public class ProfileActivity extends BaseActivity implements ProfileMVPView {


    @BindView(R.id.contentView) public LinearLayout mContent;
    @BindView(R.id.collapsing_toolbar) public CollapsingToolbarLayout mCollapsingToolbar;
    @BindView(R.id.profile_avatar) public ImageView mImageViewAvatar;

    @BindView(R.id.profile_company_name) public TextView mTextViewCompany;
    @BindView(R.id.profile_email) public TextView mTextViewEmail;
    @BindView(R.id.profile_salutation) public TextView mTextViewSalutation;
    @BindView(R.id.profile_firstname) public TextView mTextViewFirstName;
    @BindView(R.id.profile_lastname) public TextView mTextViewLastName;
    @BindView(R.id.profile_street) public TextView mTextViewStreet;
    @BindView(R.id.profile_additional_info) public TextView mTextViewAdditionalInfo;
    @BindView(R.id.profile_city) public TextView mTextViewCity;
    @BindView(R.id.profile_zip) public TextView mTextViewZip;
    @BindView(R.id.profile_country) public TextView mTextViewCountry;
    @BindView(R.id.profile_language) public TextView mTextViewLanguage;
    @BindView(R.id.profile_phone) public TextView mTextViewPhone;

    @BindView(R.id.profile_company_name_layout) public LinearLayout mLayoutCompany;
    @BindView(R.id.profile_email_layout) public LinearLayout mLayoutEmail;
    @BindView(R.id.profile_salutation_layout) public LinearLayout mLayoutSalutation;
    @BindView(R.id.profile_firstname_layout) public LinearLayout mLayoutFirstName;
    @BindView(R.id.profile_lastname_layout) public LinearLayout mLayoutLastName;
    @BindView(R.id.profile_street_layout) public LinearLayout mLayoutStreet;
    @BindView(R.id.profile_additional_info_layout) public LinearLayout mLayoutAdditionalInfo;
    @BindView(R.id.profile_city_layout) public LinearLayout mLayoutCity;
    @BindView(R.id.profile_zip_layout) public LinearLayout mLayoutZip;
    @BindView(R.id.profile_country_layout) public LinearLayout mLayoutCountry;
    @BindView(R.id.profile_language_layout) public LinearLayout mLayoutLanguage;
    @BindView(R.id.profile_phone_layout) public LinearLayout mLayoutPhone;


    @Inject
    ProfileMVPPresenterImpl presenter;

    ProfileComponent profileComponent;


    @Override
    protected boolean isDrawerEnabled() {
        return true;
    }

    @Override
    protected int onSelectedMenuId() {
        return MenuIDs.PROFILE;
    }

    @Override
    protected int onActivityTitleId() {
        return R.string.toolbar_label_profile;
    }

    @Override
    protected int onActivityLayout() {
        return R.layout.act_profile;
    }

    @Override
    protected void setupComponent() {
        profileComponent = DaggerProfileComponent.builder().profileModule(new ProfileModule(this)).build();
        profileComponent.inject(this);
    }



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showContent(false);

        presenter.onCreateView(savedInstanceState);
        if(savedInstanceState==null)
        {
            presenter.requestProfile();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstanceState(outState);
    }



    @Override
    public void updateViews(ProfileData profileData) {

        if(profileData!=null)
        {
            if(profileData.getCompanyName()!=null && !profileData.getCompanyName().isEmpty()) {
                mLayoutCompany.setVisibility(View.VISIBLE);
                mTextViewCompany.setText(profileData.getCompanyName());
            } else {
                mLayoutCompany.setVisibility(View.GONE);
            }

            if(profileData.getEmail()!=null && !profileData.getEmail().isEmpty()) {
                mLayoutEmail.setVisibility(View.VISIBLE);
                mTextViewEmail.setText(profileData.getEmail());
            } else {
                mLayoutEmail.setVisibility(View.GONE);
            }

            if(profileData.getSalutation()!=null && !profileData.getSalutation().isEmpty()) {
                mLayoutSalutation.setVisibility(View.VISIBLE);
                mTextViewSalutation.setText(profileData.getSalutation());
            } else {
                mLayoutSalutation.setVisibility(View.GONE);
            }

            if(profileData.getFirstName()!=null && !profileData.getFirstName().isEmpty()) {
                mLayoutFirstName.setVisibility(View.VISIBLE);
                mTextViewFirstName.setText(profileData.getFirstName());
            } else {
                mLayoutFirstName.setVisibility(View.GONE);
            }

            if(profileData.getLastName()!=null && !profileData.getLastName().isEmpty()) {
                mLayoutLastName.setVisibility(View.VISIBLE);
                mTextViewLastName.setText(profileData.getLastName());
            } else {
                mLayoutLastName.setVisibility(View.GONE);
            }

            if(profileData.getStreet()!=null && !profileData.getStreet().isEmpty()) {
                mLayoutStreet.setVisibility(View.VISIBLE);
                mTextViewStreet.setText(profileData.getStreet());
            } else {
                mLayoutStreet.setVisibility(View.GONE);
            }

            if(profileData.getAdditionalInfo()!=null && !profileData.getAdditionalInfo().isEmpty()) {
                mLayoutAdditionalInfo.setVisibility(View.VISIBLE);
                mTextViewAdditionalInfo.setText(profileData.getAdditionalInfo());
            } else {
                mLayoutAdditionalInfo.setVisibility(View.GONE);
            }

            if(profileData.getCity()!=null && !profileData.getCity().isEmpty()) {
                mLayoutCity.setVisibility(View.VISIBLE);
                mTextViewCity.setText(profileData.getCity());
            } else {
                mLayoutCity.setVisibility(View.GONE);
            }

            if(profileData.getZipCode()!=null && !profileData.getZipCode().isEmpty()) {
                mLayoutZip.setVisibility(View.VISIBLE);
                mTextViewZip.setText(profileData.getZipCode());
            } else {
                mLayoutZip.setVisibility(View.GONE);
            }

            if(profileData.getCountry()!=null && !profileData.getCountry().isEmpty()) {
                mLayoutCountry.setVisibility(View.VISIBLE);
                mTextViewCountry.setText(profileData.getCountry());
            } else {
                mLayoutCountry.setVisibility(View.GONE);
            }

            if(profileData.getLanguage()!=null && !profileData.getLanguage().isEmpty()) {
                mLayoutLanguage.setVisibility(View.VISIBLE);
                mTextViewLanguage.setText(profileData.getLanguage());
            } else {
                mLayoutLanguage.setVisibility(View.GONE);
            }

            if(profileData.getPhone()!=null && !profileData.getPhone().isEmpty()) {
                mLayoutPhone.setVisibility(View.VISIBLE);
                mTextViewPhone.setText(profileData.getPhone());
            } else {
                mLayoutPhone.setVisibility(View.GONE);
            }

            mCollapsingToolbar.setTitle(profileData.getFirstName()+" "+profileData.getLastName());

            Picasso.with(this)
                    .load(profileData.getAvaUrl())
                    .into(mImageViewAvatar);

            showContent(true);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_edit:
            {
                Toast.makeText(this,"Comming soon",Toast.LENGTH_SHORT).show();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showContent(boolean showContent) {
        mContent.setVisibility(showContent? View.VISIBLE:View.GONE);
    }
}
