package com.matrix42.matrix42test.ui.base;

import android.os.Bundle;

import rx.Subscription;

public interface MVPPresenter {
    void onCreateView(Bundle savedInstanceState);
    void onSaveInstanceState(Bundle outState);
    void onStop();
    void addSubscription(Subscription subscription);
}
