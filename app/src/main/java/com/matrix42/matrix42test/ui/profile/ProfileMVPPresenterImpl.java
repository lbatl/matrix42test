package com.matrix42.matrix42test.ui.profile;

import android.os.Bundle;
import android.util.Log;

import com.matrix42.matrix42test.App;
import com.matrix42.matrix42test.model.DataManager;
import com.matrix42.matrix42test.model.data.ProfileData;
import com.matrix42.matrix42test.model.mapper.ProfileMapper;
import com.matrix42.matrix42test.ui.base.BasePresenter;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by superuser on 20.10.16.
 */

public class ProfileMVPPresenterImpl extends BasePresenter<ProfileMVPView> implements ProfileMVPPresenter {

    private final static String BUNDLE_SAVE_PROFILE = "BUNDLE_SAVE_PROFILE";

    private ProfileMVPView view;

    private ProfileData profileData;

    @Inject
    DataManager dataManager;

    @Inject
    ProfileMapper profileMapper;

    @Inject
    CompositeSubscription compositeSubscription;


    @Inject
    public ProfileMVPPresenterImpl(ProfileMVPView view) {
        super();
        this.view = view;
        takeView(view);
    }

    @Override
    protected void setupPresenterComponent() {
        App.appComponent.inject(this);
    }

    @Override
    public void onCreateView(Bundle savedInstanceState) {
        if(savedInstanceState!=null)
        {
            if(savedInstanceState.containsKey(BUNDLE_SAVE_PROFILE)) profileData=savedInstanceState.getParcelable(BUNDLE_SAVE_PROFILE);
            if(profileData!=null) view.updateViews(profileData);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(BUNDLE_SAVE_PROFILE, profileData);
    }

    @Override
    public void onStop() {
        compositeSubscription.clear();
    }

    @Override
    public void addSubscription(Subscription subscription) {
        compositeSubscription.add(subscription);
    }

    @Override
    public void requestProfile() {
        if (view != null) view.showLoading();
        Subscription subscription = dataManager.getUserProfile()
                .filter(response -> response != null ? true : false)
                .map(profileMapper)
                .subscribe(profile -> {
                            this.profileData = profile;
                            view.updateViews(profileData);
                        },
                        throwable -> {
                            view.hideLoading();
                            view.showError(throwable);
                            Log.e("onError", "requestProfile " + throwable.getMessage().toString());
                        },
                        () -> {
                            view.hideLoading();
                        });

        addSubscription(subscription);
    }
}
