package com.matrix42.matrix42test.ui.login;

import android.os.Bundle;

import com.matrix42.matrix42test.App;
import com.matrix42.matrix42test.model.DataManager;
import com.matrix42.matrix42test.ui.base.BasePresenter;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by superuser on 20.10.16.
 */

public class LoginMVPPresenterImpl extends BasePresenter<LoginMVPView> implements LoginMVPPresenter
{
    private LoginMVPView view;

    @Inject
    DataManager dataManager;

    @Inject
    CompositeSubscription compositeSubscription;

    @Inject
    public LoginMVPPresenterImpl(LoginMVPView view)
    {
        super();
        this.view=view;
        takeView(view);
    }



    @Override
    public void onCreateView(Bundle savedInstanceState) {

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void onStop() {
        compositeSubscription.clear();
    }

    @Override
    public void addSubscription(Subscription subscription) {
        compositeSubscription.add(subscription);
    }

    protected void setupPresenterComponent() {
        App.appComponent.inject(this);
    }
}
