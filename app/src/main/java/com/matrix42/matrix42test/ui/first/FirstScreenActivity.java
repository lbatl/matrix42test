package com.matrix42.matrix42test.ui.first;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.matrix42.matrix42test.R;
import com.matrix42.matrix42test.di.components.DaggerFirstScreenComponent;
import com.matrix42.matrix42test.di.components.FirstScreenComponent;
import com.matrix42.matrix42test.di.modules.FirstScreenModule;
import com.matrix42.matrix42test.ui.base.BaseActivity;
import com.matrix42.matrix42test.ui.login.LoginActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by superuser on 21.10.16.
 */

public class FirstScreenActivity extends BaseActivity implements FirstScreenMVPView {

    @OnClick(R.id.first_login)
    public void onClickLogin(View v)
    {
        startActivity(this, LoginActivity.class,null,false);
    }

    @OnClick(R.id.first_register)
    public void onClickRegister(View v)
    {
        Toast.makeText(this,"Comming soon",Toast.LENGTH_SHORT).show();
    }

    @Inject
    FirstScreenMVPPresenterImpl presenter;

    FirstScreenComponent firstScreenComponent;


    @Override
    protected void setupComponent() {
        firstScreenComponent = DaggerFirstScreenComponent.builder().firstScreenModule(new FirstScreenModule(this)).build();
        firstScreenComponent.inject(this);
    }

    @Override
    protected int onActivityLayout() {
        return R.layout.act_first;
    }

    @Override
    protected int onActivityTitleId() {
        return R.string.app_name;
    }

    @Override
    protected int onSelectedMenuId() {
        return 0;
    }

    @Override
    protected boolean isDrawerEnabled() {
        return false;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.onCreateView(savedInstanceState);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstanceState(outState);
    }

}
