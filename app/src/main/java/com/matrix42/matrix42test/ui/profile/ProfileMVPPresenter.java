package com.matrix42.matrix42test.ui.profile;

import com.matrix42.matrix42test.ui.base.MVPPresenter;

/**
 * Created by superuser on 20.10.16.
 */

public interface ProfileMVPPresenter extends MVPPresenter {

    void requestProfile();
}
