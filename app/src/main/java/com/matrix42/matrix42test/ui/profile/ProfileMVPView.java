package com.matrix42.matrix42test.ui.profile;

import com.matrix42.matrix42test.model.data.ProfileData;
import com.matrix42.matrix42test.ui.base.MVPView;

/**
 * Created by superuser on 20.10.16.
 */

public interface ProfileMVPView extends MVPView {
    void updateViews(ProfileData profileData);
}
