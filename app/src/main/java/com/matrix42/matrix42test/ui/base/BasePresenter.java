package com.matrix42.matrix42test.ui.base;

import java.lang.ref.WeakReference;

public abstract class BasePresenter<V extends MVPView> implements MVPPresenter {
    private WeakReference<V> view;

    protected abstract void setupPresenterComponent();

    public BasePresenter() {
        setupPresenterComponent();
    }

    private boolean loaded = false;

    protected void takeView(V view) {
        if (view == null) throw new NullPointerException("new view must not be null");

        if (this.view != null) dropView(this.view.get());

        this.view = new WeakReference(view);
        if (!loaded) {
            loaded = true;
            onLoad();
        }
    }

    protected void dropView(V view) {
        if (view == null) throw new NullPointerException("dropped view must not be null");
        loaded = false;
        this.view = null;
        onDestroy();
    }

    protected V getView() {
        if (view == null)
            throw new NullPointerException("getView called when view is null. Ensure takeView(View view) is called first.");
        return view.get();
    }

    protected void onLoad() {
    }

    protected void onDestroy() {
    }


}