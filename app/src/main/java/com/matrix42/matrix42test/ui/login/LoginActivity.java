package com.matrix42.matrix42test.ui.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.matrix42.matrix42test.R;
import com.matrix42.matrix42test.di.components.DaggerLoginComponent;
import com.matrix42.matrix42test.di.components.LoginComponent;
import com.matrix42.matrix42test.di.modules.LoginModule;
import com.matrix42.matrix42test.ui.base.BaseActivity;
import com.matrix42.matrix42test.ui.profile.ProfileActivity;
import com.matrix42.matrix42test.utils.SharedPrefsHelper;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;

public class LoginActivity extends BaseActivity implements LoginMVPView {

    /* VIEWS*/
    @BindView(R.id.webView) public WebView mWebView;

    @Inject
    LoginMVPPresenterImpl presenter;

    LoginComponent loginComponent;

    private static final String AUTHORIZE_PATH = "https://accounts.matrix42.com/issue/oauth2/authorize?client_id=accountsmatrix42com&scope=urn:matrix42com&response_type=token&redirect_uri=https%3A%2F%2Faccounts.matrix42.com%2Fmy%2F%23%2Faccount&noanimation=1";
    private static final String REDIRECT_URI = "https://accounts.matrix42.com/my/#/account";
    private static final String CLIENT_ID = "accountsmatrix42com";
    private String accessToken;


    /*     */

    @Override
    protected boolean isDrawerEnabled() {
        return false;
    }

    @Override
    protected int onSelectedMenuId() {
        return 0;
    }

    @Override
    protected int onActivityTitleId() {
        return R.string.toolbar_label_login;
    }

    @Override
    protected int onActivityLayout() {
        return R.layout.act_login;
    }

    @Override
    protected void setupComponent() {
        loginComponent = DaggerLoginComponent.builder().loginModule(new LoginModule(this)).build();
        loginComponent.inject(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();

        presenter.onCreateView(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        presenter.onSaveInstanceState(outState);
    }

    private void initViews() {
        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setHorizontalScrollBarEnabled(false);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setSavePassword(false);

        accessToken = SharedPrefsHelper.getUserAuth();
        if (accessToken == null || accessToken.equals("")) {

            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if (url.startsWith(REDIRECT_URI)) {
                        if (url.indexOf("access_token=") != -1) {
                            accessToken = extractToken(url);
                            SharedPrefsHelper.saveUserAuth(accessToken);
                            startActivity(LoginActivity.this, ProfileActivity.class, null, true);
                        }

                        return true;
                    }

                    return super.shouldOverrideUrlLoading(view, url); // return false;
                }
            });

            String authorizationUri = returnAuthorizationRequestUri();
            mWebView.loadUrl(authorizationUri);

        } else {
            startActivity(LoginActivity.this, ProfileActivity.class, null, true);
        }
    }

    private String extractToken(String url) {
        Map<String, String> params = splitParams(url);
        if (params.containsKey("access_token")) {
            return params.get("access_token");
        } else {
            return "";
        }
    }

    public static Map<String, String> splitParams(String paramLine) {
        String url = paramLine.substring(paramLine.indexOf("?") + 1);
        Map<String, String> paramsMap = new HashMap<>();
        String[] params = url.split("&");

        for (String param : params) {
            String[] paramData = param.split("=");

            if (paramData.length < 2) {
                continue;
            }

            paramsMap.put(paramData[0], paramData[1]);
        }

        return paramsMap;
    }


    private String returnAuthorizationRequestUri() {
        StringBuilder sb = new StringBuilder();
        sb.append(AUTHORIZE_PATH);
        sb.append("?response_type=token");
        sb.append("&client_id=" + CLIENT_ID);
        sb.append("&redirect_uri=" + REDIRECT_URI);
        return sb.toString();
    }
}
