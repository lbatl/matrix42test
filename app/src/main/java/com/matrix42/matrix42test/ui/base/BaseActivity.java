package com.matrix42.matrix42test.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;

import com.matrix42.matrix42test.R;
import com.matrix42.matrix42test.ui.first.FirstScreenActivity;
import com.matrix42.matrix42test.ui.profile.ProfileActivity;
import com.matrix42.matrix42test.utils.Utils;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by superuser on 20.10.16.
 */


public abstract class BaseActivity extends AppCompatActivity implements MVPView {


    protected abstract boolean isDrawerEnabled();

    protected abstract int onSelectedMenuId();

    protected abstract int onActivityTitleId();

    protected abstract int onActivityLayout();

    protected abstract void setupComponent();

    protected BasePresenter presenter;


    protected class MenuIDs {
        public final static int PROFILE = 1;
        public final static int SIGNOUT = 2;
    }

    protected Drawer mDrawer;
    @BindView(R.id.toolbar_view) protected Toolbar mToolbar;
    @BindView(R.id.content_container) protected SwipeRefreshLayout mContentContainer;
    @BindView(R.id.coordinator_view) protected CoordinatorLayout mCoordinator;
    @BindView(R.id.appbar_view) protected AppBarLayout mAppbar;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupComponent();
        setContentView(onActivityLayout());



        ButterKnife.bind(this);

        mContentContainer.setColorSchemeResources(R.color.colorAccent);
        mContentContainer.setEnabled(false);
        mContentContainer.setProgressViewOffset(false, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 156.0f, getResources().getDisplayMetrics()));

        mToolbar.setTitle(onActivityTitleId());
        setSupportActionBar(mToolbar);
        initDrawer();

    }

    @Override
    public void onBackPressed() {
        if (isDrawerEnabled() && mDrawer != null && mDrawer.isDrawerOpen()) {
            mDrawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    private PrimaryDrawerItem prepareMenuItem(int itemId,int nameResId,int vectorIconResId,int iconColorResId, int iconSelectedColorResId)
    {
        return new PrimaryDrawerItem()
                .withName(nameResId)
                .withIcon(Utils.getColorVectorDrawable(this,vectorIconResId,iconColorResId))
                .withIdentifier(itemId)
                .withSelectedIcon(Utils.getColorVectorDrawable(this,vectorIconResId,iconSelectedColorResId));
    }

    protected void initDrawer() {
        if (isDrawerEnabled()) {
            mDrawer = new DrawerBuilder()
                    .withActivity(this)
                    .withToolbar(mToolbar)
                    .withTranslucentStatusBar(false)
                    .withSelectedItem(onSelectedMenuId())
                    .addDrawerItems(
                            prepareMenuItem(MenuIDs.PROFILE,R.string.menu_label_profile,R.drawable.v_ic_account_circle,R.color.color3,R.color.color3),
                            prepareMenuItem(MenuIDs.SIGNOUT,R.string.menu_label_signout,R.drawable.v_ic_exit,R.color.color3,R.color.color3)
                    ).build();


            mDrawer.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
            mDrawer.setOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                @Override
                public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                    closeDrawer((int) drawerItem.getIdentifier());
                    return false;
                }
            });


            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

    }


    protected void showToolbar() {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) mAppbar.getLayoutParams();
        AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
        int[] consumed = new int[2];
        if (behavior != null) {
            behavior.onNestedPreScroll(mCoordinator, mAppbar, null, 0, -1000, consumed);
        }
    }


    void closeDrawer() {
        mDrawer.closeDrawer();
    }


    void closeDrawer(final int menuItemID) {
        closeDrawer();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sideMenuItemSelected(menuItemID);
            }
        }, 250);
    }


    void sideMenuItemSelected(int menuItemID) {
        if (this.onSelectedMenuId() == menuItemID) return;

        switch (menuItemID) {
            case MenuIDs.PROFILE: {
                startActivity(this, ProfileActivity.class, null, true);
                break;
            }
            case MenuIDs.SIGNOUT:{
                Utils.logoutUser(this);
                startActivity(this, FirstScreenActivity.class, null, true);
                break;
            }
        }
    }


    protected void startActivity(BaseActivity currentAct, Class newActClass, Intent customIntent, boolean clearStack) {
        Intent intent = customIntent != null ? customIntent : new Intent(currentAct, newActClass);

        if (clearStack) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        currentAct.startActivity(intent);
        overridePendingTransition(R.anim.slide_from_bottom, R.anim.no_animation);
    }


    @Override
    protected void onStart() {
        super.onStart();
        showToolbar();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.onStop();
        }
    }

    @Override
    public void showContent(boolean showContent) {

    }

    @Override
    public void showLoading() {
        mContentContainer.setEnabled(true);
        mContentContainer.post(new Runnable() {
            @Override
            public void run() {
                mContentContainer.setRefreshing(true);
            }
        });
    }

    @Override
    public void hideLoading() {
        mContentContainer.setEnabled(false);
        mContentContainer.setRefreshing(false);
    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.slide_from_right, R.anim.no_animation);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);
        overridePendingTransition(R.anim.slide_from_right, R.anim.no_animation);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.no_animation, isDrawerEnabled() ? R.anim.slide_to_bottom : R.anim.slide_to_right);
    }
}