package com.matrix42.matrix42test.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.matrix42.matrix42test.App;

/**
 * Created by superuser on 20.10.16.
 */


public class SharedPrefsHelper {

    private static final String PREF_FILE_NAME = "prefs";
    private static final String PREF_AUTH = "USER_AUTH";


    public static SharedPreferences getPrefInstance() {
        return PreferenceManager.getDefaultSharedPreferences(App.getInstance());
    }


    public static void clear() {
        getPrefInstance().edit().clear().commit();
    }


    public static void saveUserAuth(String auth) {
        SharedPreferences.Editor e = getPrefInstance().edit();
        e.putString(PREF_AUTH, auth);
        e.commit();
    }

    public static String getUserAuth() {
        return getPrefInstance().getString(PREF_AUTH, "");
    }

    public static boolean isThirstUserAuth() {
        return getUserAuth().equals("") ? false : true;
    }



}