package com.matrix42.matrix42test.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.matrix42.matrix42test.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;

/**
 * Created by superuser on 20.10.16.
 */

public class Utils {
    public static VectorDrawableCompat getColorVectorDrawable(Context context , int vectorDrawableResId, int colorResId)
    {
        ContextThemeWrapper wrapper = new ContextThemeWrapper(context, R.style.AppTheme);
        VectorDrawableCompat drawable = VectorDrawableCompat.create(context.getResources(), vectorDrawableResId, wrapper.getTheme());
        drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(context, colorResId), PorterDuff.Mode.SRC_ATOP));
        return drawable;
    }


    public static void logoutUser(Context context)
    {
        CookieSyncManager.createInstance(context);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();

        SharedPrefsHelper.clear();
    }
}
