package com.matrix42.matrix42test.di.components;

/**
 * Created by superuser on 20.10.16.
 */


import com.matrix42.matrix42test.di.ActivityScope;
import com.matrix42.matrix42test.di.modules.ProfileModule;
import com.matrix42.matrix42test.ui.profile.ProfileActivity;

import dagger.Component;

@ActivityScope
@Component(modules = {ProfileModule.class})
public interface ProfileComponent {
    void inject(ProfileActivity activity);
}
