package com.matrix42.matrix42test.di.modules;

import com.matrix42.matrix42test.ui.first.FirstScreenMVPPresenterImpl;
import com.matrix42.matrix42test.ui.first.FirstScreenMVPView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by superuser on 21.10.16.
 */

@Module
public class FirstScreenModule {

    private FirstScreenMVPView view;

    public FirstScreenModule(FirstScreenMVPView view) {
        this.view = view;
    }

    @Provides
    FirstScreenMVPView provideFirstScreenMVPView() {
        return view;
    }

    @Provides
    FirstScreenMVPPresenterImpl provideFirstScreenMVPPresenterImpl(FirstScreenMVPView view) {
        return new FirstScreenMVPPresenterImpl(view);
    }

}