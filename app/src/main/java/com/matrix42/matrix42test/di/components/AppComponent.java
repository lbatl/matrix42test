package com.matrix42.matrix42test.di.components;

/**
 * Created by superuser on 20.10.16.
 */


import com.matrix42.matrix42test.di.modules.ModelModule;
import com.matrix42.matrix42test.di.modules.PresenterModule;
import com.matrix42.matrix42test.model.DataManagerImpl;
import com.matrix42.matrix42test.ui.first.FirstScreenMVPPresenterImpl;
import com.matrix42.matrix42test.ui.login.LoginMVPPresenterImpl;
import com.matrix42.matrix42test.ui.profile.ProfileMVPPresenterImpl;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ModelModule.class, PresenterModule.class})
public interface AppComponent {
    void inject(DataManagerImpl dataRepository);
    void inject(ProfileMVPPresenterImpl profileMVPPresenter);
    void inject(FirstScreenMVPPresenterImpl firstScreenMVPPresenter);
    void inject(LoginMVPPresenterImpl loginMVPPresenter);
}