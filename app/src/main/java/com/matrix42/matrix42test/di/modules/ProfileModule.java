package com.matrix42.matrix42test.di.modules;

import com.matrix42.matrix42test.ui.profile.ProfileMVPPresenterImpl;
import com.matrix42.matrix42test.ui.profile.ProfileMVPView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by superuser on 20.10.16.
 */

@Module
public class ProfileModule {

    private ProfileMVPView view;

    public ProfileModule(ProfileMVPView view) {
        this.view = view;
    }

    @Provides
    ProfileMVPView provideProfileMVPView() {
        return view;
    }

    @Provides
    ProfileMVPPresenterImpl provideProfileMVPPresenterImpl(ProfileMVPView view) {
        return new ProfileMVPPresenterImpl(view);
    }

}