package com.matrix42.matrix42test.di.modules;

import com.matrix42.matrix42test.ui.login.LoginMVPPresenterImpl;
import com.matrix42.matrix42test.ui.login.LoginMVPView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by superuser on 21.10.16.
 */


@Module
public class LoginModule {

    private LoginMVPView view;

    public LoginModule(LoginMVPView view) {
        this.view = view;
    }

    @Provides
    LoginMVPView provideLoginMVPView() {
        return view;
    }

    @Provides
    LoginMVPPresenterImpl provideLoginMVPPresenterImpl(LoginMVPView view) {
        return new LoginMVPPresenterImpl(view);
    }

}