package com.matrix42.matrix42test.di.components;

import com.matrix42.matrix42test.di.ActivityScope;
import com.matrix42.matrix42test.di.modules.LoginModule;
import com.matrix42.matrix42test.ui.login.LoginActivity;

import dagger.Component;

/**
 * Created by superuser on 21.10.16.
 */


@ActivityScope
@Component(modules = {LoginModule.class})
public interface LoginComponent {
    void inject(LoginActivity activity);
}
