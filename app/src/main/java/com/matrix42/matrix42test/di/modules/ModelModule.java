package com.matrix42.matrix42test.di.modules;

import com.matrix42.matrix42test.network.ApiInterface;
import com.matrix42.matrix42test.network.ApiModule;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by superuser on 20.10.16.
 */


@Module
public class ModelModule {

    @Provides
    @Singleton
    ApiInterface provideApiInterface()
    {
        return ApiModule.getApiInterface();
    }


    @Provides
    @Singleton
    @Named("UI_THREAD")
    Scheduler provideSchedulerUI() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @Singleton
    @Named("IO_THREAD")
    Scheduler provideSchedulerIO() {
        return Schedulers.io();
    }

}