package com.matrix42.matrix42test.di.components;

import com.matrix42.matrix42test.di.ActivityScope;
import com.matrix42.matrix42test.di.modules.FirstScreenModule;
import com.matrix42.matrix42test.ui.first.FirstScreenActivity;

import dagger.Component;

/**
 * Created by superuser on 21.10.16.
 */

@ActivityScope
@Component(modules = {FirstScreenModule.class})
public interface FirstScreenComponent {
    void inject(FirstScreenActivity activity);
}