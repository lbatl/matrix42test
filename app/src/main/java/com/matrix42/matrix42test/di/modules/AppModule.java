package com.matrix42.matrix42test.di.modules;

import android.app.Application;

import com.matrix42.matrix42test.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by superuser on 20.10.16.
 */


@Module
class AppModule {

    final App app;

    public AppModule(App app) {
        this.app = app;
    }


    @Provides
    @Singleton
    Application provideApplication()  {
        return app;
    }
}