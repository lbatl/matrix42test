package com.matrix42.matrix42test.di.modules;

import com.matrix42.matrix42test.model.DataManager;
import com.matrix42.matrix42test.model.DataManagerImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by superuser on 20.10.16.
 */
@Module
public class PresenterModule {

    @Provides
    @Singleton
    DataManager provideDataManagerImpl()  {
        return new DataManagerImpl();
    }

    @Provides
    CompositeSubscription provideCompositeSubscription() {
        return new CompositeSubscription();
    }

}